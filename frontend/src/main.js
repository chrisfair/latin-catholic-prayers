import 'core-js/stable';
import 'regenerator-runtime/runtime';
import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import App from './App.vue';
import store from "./store/store";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueSimpleAlert from "vue-simple-alert";

Vue.config.productionTip = false;
Vue.config.devtools = true;
import * as Wails from '@wailsapp/runtime';

Wails.Init(() => {
	Vue.use(BootstrapVue);
	Vue.use(IconsPlugin);
     Vue.use(VueSimpleAlert);
	new Vue({
		render: h => h(App),
          store
	}).$mount('#app');
});
