import Vue from "vue";
import 'es6-promise/auto';
import Vuex from "vuex";

Vue.use(Vuex);
    
export default new Vuex.Store({
     state: {
          UserData: []
     },
     mutations: {
          ChangeUserData(state, payload) {
               state.UserData = payload
          }
          
     },
     getters: {
          UserData: state => state.UserData
     }
});
