package main

import (
	"math"

	"fyne.io/fyne/v2"
)

type PrayerContainerStruct struct {
}

func (pc *PrayerContainerStruct) MinSize(objects []fyne.CanvasObject) fyne.Size {

	w, h := float32(0), float32(0)
	for _, o := range objects {
		childSize := o.MinSize()
		w += childSize.Width
		h += childSize.Height
	}

	area := float64(w) * float64(h)
	minSize := float32(math.Sqrt(area))

	return fyne.NewSize(minSize, minSize)
}

func (pc *PrayerContainerStruct) Layout(objects []fyne.CanvasObject, containerSize fyne.Size) {
	pos := fyne.NewPos(0, 0)
	sizeHolder := fyne.Size{Height: 0, Width: 0}
	for _, o := range objects {
		size := o.MinSize()
		o.Resize(size)
		o.Move(pos)
		sizeHolder.Width += size.Width

		if sizeHolder.Width <= containerSize.Width {
			pos = pos.Add(fyne.NewPos(size.Width, 0))
		} else {
			sizeHolder.Height += size.Height
			sizeHolder.Width = 0
			pos = fyne.NewPos(0, 0)
			pos = pos.Add(fyne.NewPos(0, sizeHolder.Height))
		}
	}
}
