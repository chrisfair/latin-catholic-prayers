package main

import (
	"log"
	"os"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"

	"os/user"

	"github.com/kbinani/screenshot"
	"gopkg.in/ini.v1"
)

var UserName string = "DEFAULT"

type Coordinates struct {
	width  int
	height int
}

func getResolutionOfFirstMonitor() (resolution Coordinates) {
	n := screenshot.NumActiveDisplays()
	if n > 0 {
		resolution.width = screenshot.GetDisplayBounds(0).Dx()
		resolution.height = screenshot.GetDisplayBounds(0).Dy()
	}

	if WidthOfApp > 0 {
		resolution = Coordinates{WidthOfApp, resolution.height}

	}

	if HeightOfApp > 0 {
		resolution = Coordinates{resolution.width, HeightOfApp}

	}

	return
}

func getSizeOfGraph() (graph []int) {
	sizeOfMonitor := getResolutionOfFirstMonitor()
	baseRatio := 0.7
	xint := int(float64(sizeOfMonitor.width) * baseRatio)
	yint := int(float64(sizeOfMonitor.height) * baseRatio)
	graph = []int{xint, yint}
	return
}

func GetSizeOfMovie() (movie []string) {
	sizeOfMonitor := getResolutionOfFirstMonitor()
	baseRatio := 0.65
	xstring := strconv.Itoa(int(float64(sizeOfMonitor.width) * baseRatio))
	ystring := strconv.Itoa(int(float64(sizeOfMonitor.height) * baseRatio))
	movie = []string{xstring, ystring}
	return
}
func checkAnswer(response string) (options []string) {
	lastAnswerWasCorrect = AdministerQuiz(response)
	options = GetCurrentOptions()
	if lastAnswerWasCorrect {
		go PlayWord(response)
	} else {
		go PlayErrorSound()
	}
	return
}

func getAbout() (outputString string) {
	outputString = AppName + " " + Version
	return
}

func getWrongAnswers() (answers []int) {
	wrongAnswers := GetListOfWrongAnswers()
	return wrongAnswers
}

func startQuiz(prayerName string) (options []string) {
	finalizeQuiz()
	result := InitializeQuiz(prayerName)
	if result == true {
		options = GetCurrentOptions()
	}
	return
}

// RetrievePrayer ... Public facing function to return a string array for the currently selected
// prayer as the correct prayer type
func RetrievePrayer(name string) []string {
	return RetrievePrayerArray(name)
}

// RetrievePrayerString...Public facing function to return a string for the currently selected prayer
func RetrievePrayerString(name string) string {
	return strings.Join(RetrievePrayerArray(name), " ")
}

func playPrayer(name string) {
	go PlayPrayerFile(name)
}

// PlayWord ... plays the word for the selected prayer
func PlayWord(word string) {
	if CurrentLanguageSelection == Latin {
		processedWord := StripPunctuation(word)
		processedWord = SetToLowerCase(processedWord)
		go PlayWordFromPrayer(processedWord)

	}
}

func getScore() (score string) {
	score = ReportCurrentScore()
	return

}

func getCurrentProgressOnQuiz() (reportedPrayer []string) {
	currentList := RetrievePrayerArray(SelectedPrayer)
	if len(currentList) > 0 {
		reportedPrayer = currentList[:currentQuizItem]
	}
	return
}

func getListOfGroupsForExistingScores() (reportedGroups []string) {
	user, _ := user.Current()
	userName := user.Username
	reportedGroups = GetListOfGroupsForUniquePrayers(userName)
	sort.Strings(reportedGroups)
	reportedGroups = append([]string{"All"}, reportedGroups...)
	return
}

func getListOfPrayersForExistingScores(group string) (reportedPrayers []string) {
	user, _ := user.Current()
	userName := user.Username
	reportedPrayersInGroup := GetListOfUniquePrayersThatAreInGroup(userName, group)
	reportedPrayers = append([]string{"All"}, reportedPrayersInGroup...)
	return
}
func getListOfGroups() (reportedGroups []string) {
	reportedGroups = GetListOfGroups()
	sort.Strings(reportedGroups)
	return
}

func getListOfPrayers(group string) (prayers []string) {
	prayers = GetPrayerNamesFromGroup(group)
	return
}

func getListOfPrayersWithAll(group string) (prayers []string) {

	prayers = GetPrayerNamesFromGroup(group)
	prayers = append([]string{"All"}, prayers...) // This prepends All on prayers
	return
}

func getListOfPrayersWithScores() (prayers []string) {
	user, _ := user.Current()
	prayers = GetListOfUniquePrayers(user.Username)
	return
}

// SelectUser ... Selects the User for the quiz
func SelectUser(userName string) {

	if userName == "DEFAULT" {
		setUserName()

	} else {
		UserName = userName
	}
}

// GetDefaultUser Return the default user from the config file
func GetDefaultUser() (userName string) {
	userName = ConfigurationFile.DefaultUser
	UserName = userName
	return
}

func setUserName() {
	if UserName == "DEFAULT" {
		user, err := user.Current()
		UserName = user.Username
		if runtime.GOOS == "windows" {
			if err == nil {
				splitUserName := strings.Split(user.Username, "\\")
				if len(splitUserName) > 0 {
					UserName = splitUserName[1]
				}
			}
		}
	}
}

func CreateScore(prayerName string, score string) {
	setUserName()
	currentTime := time.Now()
	dateTimeAsString := currentTime.Format(time.RFC822)
	prayerScore := PrayerScoreType{prayerName, score, dateTimeAsString}
	AddResultsToRecord(UserName, prayerScore)
}

// GetListOfUsers ... This gets a list of users for historical data
func GetListOfUsers() (list []string) {

	setUserName()
	list = append(list, UserName)
	for _, user := range ConfigurationFile.AlternateUsers {
		list = append(list, user)
	}
	return
}

func GetListOfUsersWithAll() (list []string) {
	list = ListOfUsersWithAll()
	return
}

func getCurrentScores(group string, prayer string, dateFilter string) (finalScores [][]string) {

	if UserName == "DEFAULT" {
		user, _ := user.Current()
		UserName = user.Username
	}
	finalScores = GetCurrentScores(group, UserName, prayer, dateFilter)
	return
}

func setLanguage(language string) {
	SetLanguageType(language)
}

func GetHelpFileNames() (helpFiles []string) {
	helpFiles = MyHelp.GetListOfVideos()
	return
}

func GetHelpFileName(helpFile string) (helpFileName string) {
	helpFileName = MyHelp.GetFileNameFromVideoName(helpFile)
	return

}

func GetPrayerDirectory() string {
	return ConfigurationFile.PrayerDirectory
}

func GetSoundDirectory() string {
	return ConfigurationFile.SoundDirectory
}

func GetHelpDirectory() string {
	return ConfigurationFile.HelpFiles
}

func GetUsersDirectory() string {
	return ConfigurationFile.UsersDirectory
}

func GetNumberOfOptions() string {
	return ConfigurationFile.NumberOfOptions
}

func GetAlternateUsers() []string {
	return ConfigurationFile.AlternateUsers
}

func GetDefaultGroup() string {
	return ConfigurationFile.DefaultGroup
}

func GetDefaultPrayer() string {
	return ConfigurationFile.DefaultPrayer
}

func SetGroupForConfig(group string) {
	ConfigurationFile.DefaultGroup = group
	saveConfigFile()
}

func SetPrayerForConfig(prayer string) {
	ConfigurationFile.DefaultPrayer = prayer
	saveConfigFile()
}

func SetPrayerDirectoryForConfig(prayerDirectory string) {
	ConfigurationFile.PrayerDirectory = prayerDirectory
	saveConfigFile()

}

func SetUsersDirectoryForConfig(usersDirectory string) {
	ConfigurationFile.UsersDirectory = usersDirectory
	saveConfigFile()
}

func SetNumberOfOptionsForConfig(numberOfOptions string) {
	ConfigurationFile.NumberOfOptions = numberOfOptions
	saveConfigFile()
	LoadConfiguration()
}

func SetHelpFilesForConfig(helpFiles string) {
	ConfigurationFile.HelpFiles = helpFiles
	saveConfigFile()
}

func SetSoundDirectoryForConfig(soundFiles string) {
	ConfigurationFile.SoundDirectory = soundFiles
	saveConfigFile()
}

func SetDefaultUserForConfig(user string) {
	ConfigurationFile.DefaultUser = user
	saveConfigFile()
}

func SetAlternateUsers(alternateUsers string) {
	alternateUsersSplit := strings.Split(alternateUsers, ",")
	var modifiedUsers []string
	for _, user := range alternateUsersSplit {
		tempString := strings.Trim(user, " ")
		modifiedUsers = append(modifiedUsers, tempString)

	}
	ConfigurationFile.AlternateUsers = modifiedUsers
	saveConfigFile()
}

func GetNextUpdateVersion() (update string) {
	nextVersion := getNextAvailableVersion()
	if nextVersion != "0.0.0" {
		update = nextVersion
	} else {
		update = "NO UPDATE"
	}
	return
}

// ReadOSRelease ... This reads the OS release info and returns it as a map
// if the system is linux
func LinuxDistroType() (linuxDistro string) {
	linuxDistro = "non linux"
	if runtime.GOOS == "linux" {
		cfg, err := ini.Load("/etc/os-release")
		if err != nil {
			log.Fatal("Fail to read file: ", err)
		}

		ConfigParams := make(map[string]string)
		ConfigParams["ID_LIKE"] = cfg.Section("").Key("ID_LIKE").String()

		linuxDistro = ConfigParams["ID_LIKE"]
	}
	return
}
func DownloadInstaller() (destinationFile string) {
	updateFileName, updateUrl := getNextAvailableUpdate()
	destinationFile, err := os.UserHomeDir()
	destinationFile = destinationFile + PS + "Downloads" + PS + updateFileName

	if err == nil {
		destinationFile, err = DownloadBinaryFile(destinationFile, updateUrl)
		if err != nil {
			destinationFile = "Failed to download from " + updateUrl

		}
	}
	return
}

func guiBinding() {
	if ConfigurationFile.DefaultInterface == "wails" {
		wailsBinding()
	}

	if ConfigurationFile.DefaultInterface == "fyne" {
		fyneBinding()
	}
}
