package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

type GraphicalUserInterface struct {
	UserSelect     *widget.Select
	GroupSelect    *widget.Select
	PrayerSelect   *widget.Select
	LanguageSelect *widget.Select
	PrayerDisplay  *PrayerControl
	UsersString    string
}

func (ui *GraphicalUserInterface) makeGraphTab() (graphTab *container.TabItem) {
	graphContainer := container.NewVBox()
	graphTab = container.NewTabItem("Graph", graphContainer)
	return
}

func (ui *GraphicalUserInterface) makeScoresTab() (scoresTab *container.TabItem) {
	scoresContainer := container.NewVBox()
	scoresTab = container.NewTabItem("Graph", scoresContainer)
	return
}

func (ui *GraphicalUserInterface) makeUserScoresTab() (userScoresTab *container.TabItem) {
	graphTab := ui.makeGraphTab()
	scoresTab := ui.makeScoresTab()
	userScoresContainer := container.NewAppTabs(scoresTab, graphTab)
	userScoresTab = container.NewTabItem("User Scores", userScoresContainer)
	return
}

func (ui *GraphicalUserInterface) makeConfigurationTab() (configurationTab *container.TabItem) {
	configurationContainer := container.NewGridWithRows(5)
	configurationTab = container.NewTabItem("Configuration", configurationContainer)
	return
}

func (ui *GraphicalUserInterface) makeHelpTab() (helpTab *container.TabItem) {
	helpContainer := container.NewGridWithRows(5)
	helpTab = container.NewTabItem("Help", helpContainer)
	return
}

func fyneBinding() (ui *GraphicalUserInterface) {
	newUI := GraphicalUserInterface{}
	ui = &newUI
	myApp := app.New()
	myWindow := myApp.NewWindow("LaCap")
	prayerTab := ui.makePrayerTab()
	userScoresTab := ui.makeUserScoresTab()
	configurationTab := ui.makeConfigurationTab()
	helpTab := ui.makeHelpTab()

	tabControl := container.NewAppTabs(prayerTab, userScoresTab, configurationTab, helpTab)

	myWindow.SetContent(tabControl)
	myWindow.ShowAndRun()
	return
}
