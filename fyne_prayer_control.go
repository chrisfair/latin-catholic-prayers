package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

// the button including the function to execute when
// playing the word
type PrayerWord struct {
	wordWidget *widget.Button
	Word       string
}

// NewPrayerWord ... Constructor for a new prayer word
func NewPrayerWord(word string) (prayerWord *PrayerWord) {

	prayerWord = &PrayerWord{}

	var readWord = func() {
		PlayWord(word)
	}

	prayerWord.wordWidget = widget.NewButton(word, readWord)

	return
}

func (pw *PrayerWord) ReadWord(word string) {
	PlayWord(word)
}

// PrayerControl ... This is the widget for containing
// all of the buttons for the prayer control and handle the
//
type PrayerControl struct {
	Prayer          []string
	prayerWords     []*PrayerWord
	PrayerContainer *fyne.Container
	TypeOfButton    string
}

// NewPrayerControl ... This creates a prayer control that has clickable elements
func NewPrayerControl(container *fyne.Container, buttonType string) (pc *PrayerControl) {

	pc = &PrayerControl{}

	if container != nil {
		pc.PrayerContainer = container
	}
	pc.TypeOfButton = buttonType
	return
}

// ChangePrayer ... This changes the prayer that is loaded into the control.
func (pc *PrayerControl) ChangePrayer(prayer []string) {

	if prayer == nil {
		prayer = []string{""}
	}
	pc.Prayer = prayer

	for _, object := range pc.PrayerContainer.Objects {
		pc.PrayerContainer.Remove(object)
	}
	pc.prayerWords = nil

	for _, word := range prayer {
		newButton := NewPrayerWord(word)
		pc.PrayerContainer.AddObject(newButton.wordWidget)

	}
}
