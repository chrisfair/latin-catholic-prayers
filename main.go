package main

import (
	"flag"
	"fmt"
)

// Version ... Contains the version information for the app
const Version string = "1.0.4"

// AppName ... Contains the App name
const AppName string = "LaCap"

var WidthOfApp int = 0
var HeightOfApp int = 0

func main() {
	InitializeSpeaker() // I do not want to re-iniatialize the speaker over and over again
	cli := flag.Bool("cli", false, "This enables a cli interface instead of the gui.")
	versionFlag := flag.Bool("version", false, "This will return the application version.")
	gui := flag.String("gui", "wails", "This will set the gui")
	width := flag.Int("width", 0, "This is the default width of the app.")
	height := flag.Int("height", 0, "This is height of the app.")
	flag.Parse()

	WidthOfApp = *width
	HeightOfApp = *height
	Gui := *gui
	configIsValid := LoadConfiguration()

	if configIsValid {
		if (*cli == false) && (*versionFlag == false) && (*gui == "") {
			// wailsBinding is in the file frontend_interface.go
			if ConfigurationFile.DefaultInterface == "" {
				ConfigurationFile.DefaultInterface = "wails"
			}
			guiBinding() // This exposes the application to the web frontend through wails
		} else if Gui == "wails" || Gui == "fyne" && *cli != true {
			ConfigurationFile.DefaultInterface = Gui
			guiBinding()
		} else if *versionFlag == true {
			fmt.Println(Version)
		} else if *cli == true {
			fmt.Println("Do Cli") // eventually this will expose the cli interface

		}
	}
}
