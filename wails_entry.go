package main

import (
	_ "embed"

	"github.com/wailsapp/wails"
)

//go:embed frontend/dist/app.js
var js string

//go:embed frontend/dist/app.css
var css string

func wailsBinding() {

	resolution := getResolutionOfFirstMonitor()

	app := wails.CreateApp(&wails.AppConfig{
		Width:  resolution.width,
		Height: resolution.height,
		Title:  getAbout(),
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})

	app.Bind(startQuiz)
	app.Bind(checkAnswer)
	app.Bind(getScore)
	app.Bind(getCurrentProgressOnQuiz)
	app.Bind(RetrievePrayer)
	app.Bind(playPrayer)
	app.Bind(PlayWord)
	app.Bind(getListOfGroups)
	app.Bind(getListOfPrayers)
	app.Bind(setLanguage)
	app.Bind(getWrongAnswers)
	app.Bind(getAbout)
	app.Bind(CreateScore)
	app.Bind(getCurrentScores)
	app.Bind(GetListOfUsers)
	app.Bind(getListOfPrayersWithAll)
	app.Bind(getListOfPrayersWithScores)
	app.Bind(getListOfGroupsForExistingScores)
	app.Bind(getListOfPrayersForExistingScores)
	app.Bind(getSizeOfGraph)
	app.Bind(GetHelpFileNames)
	app.Bind(GetHelpFileName)
	app.Bind(GetSizeOfMovie)
	app.Bind(ClearResults)
	app.Bind(GetPrayerDirectory)
	app.Bind(GetSoundDirectory)
	app.Bind(GetHelpDirectory)
	app.Bind(GetUsersDirectory)
	app.Bind(GetNumberOfOptions)
	app.Bind(GetAlternateUsers)
	app.Bind(SetGroupForConfig)
	app.Bind(SetPrayerForConfig)
	app.Bind(GetDefaultGroup)
	app.Bind(GetDefaultPrayer)
	app.Bind(SetPrayerDirectoryForConfig)
	app.Bind(SetUsersDirectoryForConfig)
	app.Bind(SetNumberOfOptionsForConfig)
	app.Bind(SetHelpFilesForConfig)
	app.Bind(SetSoundDirectoryForConfig)
	app.Bind(SetAlternateUsers)
	app.Bind(GetListOfUsers)
	app.Bind(SelectUser)
	app.Bind(SetDefaultUserForConfig)
	app.Bind(GetDefaultUser)
	app.Bind(GetNextUpdateVersion)
	app.Bind(LinuxDistroType)
	app.Bind(DownloadInstaller)
	app.Run()
}
