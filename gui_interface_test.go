package main

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_getListOfGroupsForExistingScores(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	groups := getListOfGroupsForExistingScores()
	assert.Equal(t, len(groups), 1)
	assert.Contains(t, groups, "All")
	assert.NotContains(t, groups, "Devil")

}
func Test_getListOfPrayerWithAll(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	prayers := getListOfPrayersWithAll("Rosary")
	assert.Equal(t, len(prayers), 8)
	assert.Contains(t, prayers, "All")

}

func Test_GetHelpFileNames(t *testing.T) {
	ConfigurationFile.HelpFiles = "HelpFiles"
	helpFiles := GetHelpFileNames()
	assert.Equal(t, len(helpFiles), 9)
	assert.Contains(t, helpFiles, "Introduction")
	assert.NotContains(t, helpFiles, "Devil")

}

func Test_GetHelpFileName(t *testing.T) {
	ConfigurationFile.HelpFiles = "HelpFiles"
	helpFile := GetHelpFileName("Introduction")
	assert.Contains(t, helpFile, "mp4")
	assert.NotContains(t, helpFile, "Devil")

}

func Test_GetListOfUsers(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	users := GetListOfUsers()
	assert.Equal(t, len(users), 1)
	assert.NotContains(t, users, "supergirl")
}

func Test_CreateScore(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserTestingUsers"
	CreateScore("Pater Noster", "33.3%")
	scores := getCurrentScores("All", "Pater Noster", "All")
	assert.Contains(t, scores[0], "Pater Noster")
	assert.Contains(t, scores[0], "33.3%")
	os.RemoveAll(ConfigurationFile.UsersDirectory)
}

func Test_getListOfPrayersForExistingScores(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	prayers := getListOfPrayersForExistingScores("Rosary")
	assert.Equal(t, len(prayers), 1)
	assert.Contains(t, prayers, "All")
	assert.NotContains(t, prayers, "Devil Prayer")
}

func Test_getListOfPrayersWithScores(t *testing.T) {
	ConfigurationFile.PrayerDirectory = "Prayers"
	ConfigurationFile.UsersDirectory = "UserResultsTest"
	prayers := getListOfPrayersWithScores()
	assert.NotContains(t, prayers, "Devil Prayer")
}

func Test_getSizeOfGraph(t *testing.T) {
	sizeOfMonitor := getResolutionOfFirstMonitor()
	sizeOfGraph := getSizeOfGraph()
	sizeOfMovie := GetSizeOfMovie()

	heightOfMovie, htError := strconv.Atoi(sizeOfMovie[1])
	widthOfMovie, wtError := strconv.Atoi(sizeOfMovie[0])

	assert.Greater(t, sizeOfMonitor.height, sizeOfGraph[1])
	assert.Greater(t, sizeOfMonitor.width, sizeOfGraph[0])
	assert.Greater(t, sizeOfMonitor.width, widthOfMovie)
	assert.Greater(t, sizeOfMonitor.height, heightOfMovie)

	assert.NoError(t, htError)
	assert.NoError(t, wtError)
}

func Test_getCurrentProgressOnQuiz(t *testing.T) {
	progress := getCurrentProgressOnQuiz()
	assert.Equal(t, len(progress), 0)
}

func Test_getScore(t *testing.T) {
	score := getScore()
	assert.Contains(t, score, "%")

}

func Test_getResolutionOfFirstMonitor(t *testing.T) {
	coords := getResolutionOfFirstMonitor()
	assert.Greater(t, coords.height, 0, "Should be higher than 0")
	assert.Greater(t, coords.width, 0, "Should be higher than 0")
}

func Test_getAbout(t *testing.T) {
	testString := getAbout()
	assert.Contains(t, testString, Version, "correct version not found")
	assert.Contains(t, testString, AppName, "Should contain appname in the output")
}

func Test_getListOfGroups(t *testing.T) {
	loadConfigurationFromWorkingDir()
	testString := getListOfGroups()
	assert.Contains(t, testString, "Basic", "Should contain Basic")
	assert.Contains(t, testString, "All", "Should contain All")
}

func Test_getListOfPrayers(t *testing.T) {
	loadConfigurationFromWorkingDir()
	testString := getListOfPrayers("All")
	assert.Contains(t, testString, "Pater Noster", "Should contain Pater Noster")
	assert.NotContains(t, testString, "Devil Prayer", "Should not contain Devil Prayer")
}

func Test_setLanguage(t *testing.T) {
	setLanguage("English")
	assert.Equal(t, CurrentLanguageSelection, English, "Set to Engish should be English")

}

func Test_startQuiz(t *testing.T) {
	loadConfigurationFromWorkingDir()
	setLanguage("Latin")
	testStringArray := startQuiz("Pater Noster")
	assert.Contains(t, testStringArray, "pater")
}

func Test_RetrievePrayer(t *testing.T) {
	loadConfigurationFromWorkingDir()
	testString := RetrievePrayer("Pater Noster")
	assert.Contains(t, testString, "Pater", "Must contain the word Pater")
}

func Test_checkAnswer(t *testing.T) {
	loadConfigurationFromWorkingDir()
	setLanguage("Latin")
	startQuiz("Pater Noster")
	testStringArray := checkAnswer("pater")
	assert.Contains(t, testStringArray, "noster")
	testStringArray = checkAnswer("notinarray")
	assert.Contains(t, testStringArray, "noster")
	testIntArray := getWrongAnswers()
	assert.Contains(t, testIntArray, 1, "Should have one wrong answer in the array somewhere")

}
